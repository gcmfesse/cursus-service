def call (Map vars) {
    // call funtion example: createDeploymentConfig(
    //                              project: 'name of project where to deploy',
    //                              appName: 'the application name',
    //                              envVar0: 'MAIL_MAINEMAILADRES=elinsen@kza.nl',
    //                              envVar1: 'IMAGE=IMAGE=${projectO}/${appName}:latest',
    //                              envVar2: 'etc',
    //                              envVar3: 'etc',
    //                              envVar4: 'etc',
    //                              envVar5: 'etc',
    //                              envVar6: 'etc',
    //                              envVar7: 'etc',
    //                              envVar8: 'etc',
    //                              envVar9: 'etc'
    //                              )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            def models = openshift.process("--filename=openshift/${vars.appName}-deployconfig.yml",
                    "-p",
                    vars.envVar0 ?: " ",
                    vars.envVar1 ?: " ",
                    vars.envVar2 ?: " ",
                    vars.envVar3 ?: " ",
                    vars.envVar4 ?: " ",
                    vars.envVar5 ?: " ",
                    vars.envVar6 ?: " ",
                    vars.envVar7 ?: " ",
                    vars.envVar8 ?: " ",
                    vars.envVar9 ?: " "
            )
            openshift.apply(models)
        }
    }
}