package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status,Long> {

    public Status findByStatus(String status);
}
